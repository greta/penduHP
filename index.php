<?php

/* fonction permet de afficher les tirets selon le 
 * nombre de caractères dans le mot
 */

function afficher_fonction($mot) {
    
    $tiret = array();
    $len = strlen($mot);

    for ($i = 0; $i < $len; $i++) {
        $tiret[] = "_";
    }

    for ($i = 0; $i < $len; $i++) {
        echo $tiret[$i] . PHP_EOL;
    }
    return $tiret;
}
